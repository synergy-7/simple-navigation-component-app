package com.example.simplenavigationview

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.simplenavigationview.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {
    /*
    * FirstFragment sebagai fragment launcher, anda bisa set di main_navigation
    * Klik tombol home, atau tambah startDestination
    *   Lihat main_navigation baris 6
    * */
    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnToSecond.setOnClickListener {
            /*
            * Anda bisa passing data dengan cara melalui bundle seperti dibawah
            * Atur navcontroller dengan membawa bundle.
            * */
            val data = binding.etArgsText.text.toString()
            val bundle = Bundle()
            bundle.putString(MESSAGE, data)

            findNavController().navigate(R.id.secondFragment, bundle)
        }
    }

    companion object {
        const val MESSAGE = "MESSAGE"
    }
}