package com.example.simplenavigationview

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.simplenavigationview.databinding.FragmentFirstBinding
import com.example.simplenavigationview.databinding.FragmentSecondBinding

class SecondFragment : Fragment() {
    private var _binding: FragmentSecondBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /*
        * Anda bisa mengambil data yang di bawa oleh bundle dengan cara berikut
        * */
        binding.textArgsText.text = arguments?.getString(FirstFragment.MESSAGE)

        binding.btnToThird.setOnClickListener {
            val data = binding.textArgsText.text.toString()

            /*
            * Selain anda membawa data dari bundle anda bisa menggunakan safeArgs.
            * Syaratnya anda harus menambahkan plugins di gradle level project (baris 4) dan level app (baris 4)
            * kemudian tambahkan arguments di Fragment tujuannya
            * Silakan buka di main_navigation baris 28
            * Anda bisa membawa data dengan cara memanggil object argumentnya seperti dibawah.
            * */
            val action = SecondFragmentDirections.actionSecondFragmentToThirdFragment()
            action.pesan = data
            findNavController().navigate(action)
        }
    }

}