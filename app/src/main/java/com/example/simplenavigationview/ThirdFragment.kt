package com.example.simplenavigationview

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.simplenavigationview.databinding.FragmentSecondBinding
import com.example.simplenavigationview.databinding.FragmentThirdBinding

class ThirdFragment : Fragment() {
    private var _binding: FragmentThirdBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentThirdBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        /*
        * Untuk dapat mengambil data dari safeArgs
        * Anda bisa melihat seperti berikut.
        * */
        super.onViewCreated(view, savedInstanceState)
        val data = ThirdFragmentArgs.fromBundle(arguments as Bundle).pesan
        binding.thirdFragmentText.setText(data)
    }
}