package com.example.simplenavigationview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*
        * Untuk menggunakan navigation component
        * Anda harus menambahkan library di gradle,
        *   Lihat gradle level module:app baris 4, 48, 49
        * Anda harus menambahkan library di gradle,
        *   Lihat gradle level project baris 4
        *
        * Buatlah navigation seperti yang ada di folder navigation
        * open main_navigation
        * */
    }
}